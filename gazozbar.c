#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <time.h>
#include <string.h>


static const char *icons[] =
{
	"",
	"","","","","","","",
	"","","","","","","","","","",
	"婢","奄","奔","墳",
	""
};


static Display *display;

static char *memory_str;
static char *date_str;
static char *battery_str;
static char *pulse_str;


static char *read_file(const char *path)
{
	char *str;
	FILE *fp;
	size_t length;

	fp = fopen(path, "r");

	if(fp == NULL)
	{
		fprintf(stderr, "Error failed to read:%s\n", path);
		return NULL;
	}
	
	fseek(fp, 0, SEEK_END);
	length = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	str = malloc(length+1);
	memset(str,'\0',length+1);
	fread(str, 1, length, fp);
	fclose(fp);

	return str;
}


static int read_file_int(const char *path)
{
	int v;
	char *str;
	str = read_file(path);
	sscanf(str, "%d", &v);
	free(str);
	return v;
}






static char *get_time()
{

	char *str;
	char buf[50];
	memset(buf, '\0', sizeof(buf));
	time_t t;
	struct tm *tm;
	int str_len;

	t = time(NULL);
	tm = localtime(&t);
	
	strftime(buf, sizeof(buf), "%d %B %Y - %H:%M", tm);
	
	str_len = strlen(buf) + 3;
	str = malloc(str_len + 1);

	str[str_len] = '\0';

	sprintf(str, "%s %s", icons[0], buf);
	return str;
}


static char* get_battery()
{
	char *str;
	char *status;
	int capacity;
	int icon;


	status = read_file("/sys/class/power_supply/BAT0/status");
	capacity = read_file_int("/sys/class/power_supply/BAT0/capacity");

	if(strcmp("Discharging\n", status) == 0)
	{
		if(capacity <= 10)      {icon = 8;}
		else if(capacity <= 20) {icon = 9;}
		else if(capacity <= 30) {icon = 10;}
		else if(capacity <= 40) {icon = 11;}
		else if(capacity <= 50) {icon = 12;}
		else if(capacity <= 60) {icon = 13;}
		else if(capacity <= 70) {icon = 14;}
		else if(capacity <= 80) {icon = 15;}
		else if(capacity <= 90) {icon = 16;}
		else                    {icon = 17;}

	}
	else
	{	
		if(capacity <= 20)      {icon = 1;}
		else if(capacity <= 30) {icon = 2;}
		else if(capacity <= 40) {icon = 3;}
		else if(capacity <= 60) {icon = 4;}
		else if(capacity <= 80) {icon = 5;}
		else if(capacity <= 90) {icon = 6;}
		else                    {icon = 7;}
	}
	free(status);

	str = malloc(8);
	memset(str,'\0',sizeof(str));
	sprintf(str, "%s %d%%", icons[icon], capacity);

	return str;
}

static void get_pulse(int dummy)
{

	char *str;
	FILE *fp;
	int volume;
	char buf[10];
	int icon;

	fp = popen("/bin/pamixer --get-volume", "r");
	fscanf(fp, "%d", &volume); 
	pclose(fp);
	fp = NULL;


	fp = popen("/bin/pamixer --get-volume-human", "r");
	fscanf(fp, "%s", buf); 
	pclose(fp);

	if(strcmp(buf, "muted") == 0)
	{
		icon = 18;
	}
	else
	{
		if(volume <= 33)
		{
			icon = 19;
		}
		else if (volume <= 66)
		{
			icon = 20;
		} 
		else
		{
			icon = 21;
		}
	}

	str = malloc(8);
	str[7] = '\0';
	sprintf(str, "%s %d%%", icons[icon], volume);
	if(pulse_str != NULL)
	{
		free(pulse_str);
	}
	pulse_str = str;
}

static char *get_memory()
{
	char *str;
	FILE *fp;

	float total;
	float used;
	int tmp;

	fp = popen("free  | grep Mem | awk '{print $2}'", "r");
	fscanf(fp, "%d", &tmp); 
	pclose(fp);
	total = tmp*1.0f/1024/1024;
	
	fp = popen("free  | grep Mem | awk '{print $3}'", "r");
	fscanf(fp, "%d", &tmp); 
	pclose(fp);
	used = tmp*1.0f/1024/1024;

	str = malloc(20);
	memset(str, '\0', 10);

	sprintf(str, "%s %.2fG/%.2fG", icons[22], used, total);
	return str;
}


static void set_status()
{
	char str[100];


	date_str = get_time();
	battery_str = get_battery();
	memory_str = get_memory();


	sprintf(str, "%s | %s | %s | %s", memory_str, pulse_str, battery_str, date_str);
	
	free(memory_str);
	free(date_str);
	free(battery_str);

	XStoreName(display, DefaultRootWindow(display), str);
	XSync(display, False);
}


int main()
{

	get_memory();
	
	display = XOpenDisplay(NULL);
	if(!display)
	{
		fprintf(stderr, "Cannot open display!\n");
		return 1;
	}

	signal(SIGRTMIN+2, get_pulse);
	get_pulse(0);
	while(1)
	{
		set_status();
		sleep(10);
	}

	return 0;
}
